---
layout: election
title: Elecciones municipales en Brasil
type: Regionales y municipales
country: Brasil
date_election: 2020-08-04
---
Las elecciones municipales de Brasil de 2020 se llevarán a cabo el 4 de octubre, con el sistema electoral a dos vueltas, con el segundo vuelta se llevará a cabo el 28 de octubre. Más de 150 millones de electores escogerán los alcaldes (prefectos), vicealcaldes (viceprefectos) y concejales (vereadores) de los 5.568 municipios de Brasil.