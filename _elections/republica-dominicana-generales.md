---
layout: election
title: Elecciones generales en República Dominicana
type: Presidenciales y Legislativas
country: República Dominicana
date_election: 2020-05-17
---
La elección general permitirá votar por presidente, vicepresidente, diputados, senadores, alcaldes y regidores, entre otros cargos, lo que implica una renovación completa de los poderes Ejecutivo y Legislativo.

República Dominicana también elegirá nuevo presidente en 2020. Tras dos períodos del presidente Danilo Medina, los dominicanos deberán optar entre Guillermo Castillo, del oficialista Partido de la Liberación Dominicana, el empresario Luis Abinader del Partido Revolucionario Moderno y el expresidente Leonel Fernández de Fuerza del Pueblo, entre otros.

Ese mismo día, los dominicanos también votarán para renovar el Congreso: 190 miembros de la Cámara de Diputados y 32 senadores. Actualmente, ambas cámaras están dominadas por políticos del PLD.
Asimismo, se escogerán 20 legisladores que irán al Parlamento Centroamericano (Parlacen), del que República Dominicana, pese a ser un país del Caribe, forma parte.

Sumado a ello, los dominicanos elegirán a 158 alcaldes y 1.164 regidores (concejales), para un período de cuatro años de mandato.