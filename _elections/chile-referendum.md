---
layout: election
title:  Referéndum constitucional en Chile
type: Plebiscito
country: Chile
date_election: 2020-04-26
---
El proceso legal para convocar a una reforma constitucional fue una de las consecuencias principales de las protestas desatadas en Chile en octubre de 2019. En esa consulta, también decidirán sobre el mecanismo, entre dos planteados, que prefieren para la elaboración de la nueva Constitución: una convención constituyente compuesta en un 100 % por ciudadanos y la otra, una mixta conformada 50 % por parlamentarios y 50 % por miembros electos para este fin.

En el plebiscito, los chilenos deberán optar entre dos papeletas: una con la pregunta "¿Quiere usted una nueva Constitución?" y las posibles respuestas "apruebo" o "rechazo" y una segunda hoja en que se pregunta sobre "¿Qué tipo de órgano debiera redactar la nueva Constitución?", pudiendo escoger una "Convención Mixta Constitucional" o una "Convención Constitucional".