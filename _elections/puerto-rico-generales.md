---
layout: election
title: Elecciones generales en Puerto Rico
type: Presidenciales y Legislativas
country: Puerto Rico
date_election: 2020-11-03
---
Con la ratificación de la candidatura del senador por acumulación del Partido Independentista Puertorriqueño (PIP) Juan Dalmau Ramírez, son nueve los candidatos que, a día de hoy, aspiran a la gobernación.

Entre los aspirantes, hay oficialmente cuatro precandidatos del Partido Popular Democrático; dos candidatos independientes; y uno respectivamente por el Partido Nuevo Progresista, Movimiento Victoria Ciudadana y Partido Independentista Puertorriqueño.