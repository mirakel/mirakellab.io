---
layout: election
title: Elecciones Congresales Extraordinarias
type: Legislativas
country: Perú
date_election: 2020-01-26
---
Son comicios extraordinarios porque se llevan a cabo tras la disolución, el pasado 30 de septiembre, del Congreso, por parte del presidente Martín Vizcarra.

Los peruanos deberán elegir a 130 nuevos congresistas para que completen el período que se inició originalmente en 2016.