---
layout: election
title: Elecciones de integrantes de la Asamblea Constituyente en Chile
type: Regionales y municipales
country: Chile
date_election: 2020-08-25
---
Una vez que los chilenos definan si quieren una reforma constitucional y qué tipo de convención desean, la ciudadanía deberá regresar a las urnas para votar a los integrantes del órgano encargado de la redacción de una nueva Constitución.

Además, por primera vez elegirán, mediante el voto popular, a los gobernadores regionales, 16 en total, que estarán a cargo de igual número de regiones chilenas; hasta ahora ese puesto era de un intendente, quien era nombrado por el Presidente de la República, pero cambió a partir de la publicación de la Ley 21.073 de 2018.

También serán electos 345 alcaldes y 2.240 concejales, para el período gubernamental de diciembre de 2020 a diciembre de 2024.